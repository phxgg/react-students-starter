import { useEffect, useState } from "react";
import { Col, Nav, Row, Tab } from "react-bootstrap";
import { useTranslation } from "react-i18next";
import { useLocation, useNavigate } from "react-router";

export function GradesTabs() {
  const { t } = useTranslation();
  const location = useLocation();
  const navigate = useNavigate();
  const path = location.pathname;
  const [key, setKey] = useState(null);

  useEffect(() => {
    setKey(getKeyFromPath(path));
  }, [path])

  function handleSelect(key) {
    setKey(key);
    switch (key) {
      case 'recent':
        navigate('/grades/recent');
        break;
      case 'all':
        navigate('/grades/all');
        break;
      case 'theses':
        navigate('/grades/theses');
        break;
      default:
        break;
    }
  }

  function getKeyFromPath(path) {
    if (path.endsWith('/recent')) {
      return 'recent';
    }
    if (path.endsWith('/all')) {
      return 'all';
    }
    if (path.endsWith('/theses')) {
      return 'theses';
    }
    return 'recent';
  }

  return (
    <Tab.Container id="grade-tabs" activeKey={key} onSelect={handleSelect}>
      <Row className="mb-3">
        <Col>
          <Nav variant={"tabs"} className="flex-row">
            <Nav.Item>
              <Nav.Link eventKey="recent">
                {t('Recent Grades')}
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link eventKey="all">
                {t('All Grades')}
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link eventKey="theses">
                {t('Theses')}
              </Nav.Link>
            </Nav.Item>
          </Nav>
        </Col>
      </Row>
    </Tab.Container>
  );
}
