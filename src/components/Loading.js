import { Spinner } from "react-bootstrap";

export function Loading() {
  return (
    <Spinner animation="grow" role="status" className="m-3">
      <span className="sr-only">Loading...</span>
    </Spinner>
  );
}
